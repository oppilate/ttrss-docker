#!/bin/sh -e

while ! pg_isready -h $TTRSS_DB_HOST -U $TTRSS_DB_USER; do
	echo waiting until $TTRSS_DB_HOST is ready...
	sleep 3
done

# We don't need those here (HTTP_HOST would cause false SELF_URL_PATH check failures)
unset HTTP_PORT
unset HTTP_HOST

if ! id app >/dev/null 2>&1; then
	addgroup -g $OWNER_GID app
	adduser -D -h /var/www/html -G app -u $OWNER_UID app
fi

update-ca-certificates || true

DST_DIR=/var/www/html/tt-rss
SRC_REPO=https://git.tt-rss.org/fox/tt-rss.git

[ -e $DST_DIR ] && rm -f $DST_DIR/.app_is_ready

export PGPASSWORD=$TTRSS_DB_PASS

[ ! -e /var/www/html/index.php ] && cp ${SCRIPT_ROOT}/index.php /var/www/html

PSQL="psql -q -h $TTRSS_DB_HOST -U $TTRSS_DB_USER $TTRSS_DB_NAME"

if [ ! -d $DST_DIR/.git ]; then
	mkdir -p $DST_DIR
	echo cloning tt-rss source from $SRC_REPO to $DST_DIR...
	git clone $SRC_REPO $DST_DIR || echo error: failed to clone master repository.
else
	echo updating tt-rss source in $DST_DIR from $SRC_REPO...
	cd $DST_DIR &&
		git config core.filemode false &&
		git config pull.rebase false &&
		git pull origin master || echo error: unable to update master repository.
fi

if [ ! -e $DST_DIR/index.php ]; then
	echo "error: tt-rss index.php missing (git clone failed?), unable to continue."
	exit 1
fi

## Plugins

if [ ! -d $DST_DIR/plugins.local/nginx_xaccel ]; then
	echo cloning plugins.local/nginx_xaccel...
	git clone https://git.tt-rss.org/fox/ttrss-nginx-xaccel.git \
		$DST_DIR/plugins.local/nginx_xaccel ||  echo warning: failed to clone nginx_xaccel.
else
	if [ -z "$TTRSS_NO_STARTUP_PLUGIN_UPDATES" ]; then
		echo updating all local plugins...

		find $DST_DIR/plugins.local/ -maxdepth 1 -mindepth 1 -type d | while read PLUGIN; do
			echo updating $PLUGIN...

			cd $PLUGIN && \
				git config core.filemode false && \
				git config pull.rebase false && \
			  	git pull origin master || echo warning: attempt to update plugin $PLUGIN failed.
		done
	else
		echo updating plugins.local/nginx_xaccel...
		cd $DST_DIR/plugins.local/nginx_xaccel && \
			git config core.filemode false && \
			git config pull.rebase false && \
		  	git pull origin master || echo warning: attempt to update plugin nginx_xaccel failed.
	fi
fi

init_plugin_theme() {
	NAME=$1
	GIT_REPOSITORY=$2
	TYPE=$3

	if [ $TYPE == "plugins" ]; then
		if [ ! -d $DST_DIR/$TYPE.local/$NAME ]; then
			echo cloning $TYPE.local/$NAME...
			git clone $GIT_REPOSITORY \
				$DST_DIR/$TYPE.local/$NAME || echo error: failed to clone $NAME repository.
		else
			echo updating $TYPE.local/$NAME...
			cd $DST_DIR/$TYPE.local/$NAME &&
				git config core.filemode false &&
				git config pull.rebase false &&
				git pull origin master || echo error: failed to update $NAME repository.
		fi
	else
		echo error: unknown type $TYPE
	fi
}
init_plugin() {
	init_plugin_theme $1 $2 plugins
}

# Fever
init_plugin fever https://github.com/DigitalDJ/tinytinyrss-fever-plugin.git

# Mercury Fulltext
init_plugin mercury_fulltext https://github.com/HenryQW/mercury_fulltext.git

# Feediron
init_plugin feediron https://github.com/feediron/ttrss_plugin-feediron.git

# OpenCC
init_plugin opencc https://github.com/HenryQW/ttrss_opencc.git

# News+ API
init_plugin api_newsplus https://github.com/voidstern/tt-rss-newsplus-plugin.git

# FeedReader API
{
	NAME=api_feedreader
	URL=https://raw.githubusercontent.com/jangernert/FeedReader/master/data/tt-rss-feedreader-plugin/api_feedreader/init.php

	echo downloading plugins.local/$NAME...
	mkdir $DST_DIR/plugins.local/$NAME -p
	wget $URL -O $DST_DIR/plugins.local/$NAME/init.php || echo error: failed to download $NAME plugin repository.
}

# Options per feed
init_plugin options_per_feed https://github.com/sergey-dryabzhinsky/options_per_feed.git

# Remove iframe sandbox
init_plugin remove_iframe_sandbox https://github.com/DIYgod/ttrss-plugin-remove-iframe-sandbox.git

# Wallabag
init_plugin wallabag_v2 https://github.com/joshp23/ttrss-to-wallabag-v2.git

## Plugins End

## Themes

# Feedly
apk add tar # for tar --wildcards
wget -qO- https://github.com/levito/tt-rss-feedly-theme/archive/master.tar.gz |
	tar xzvpf - --strip-components=1 --wildcards -C . tt-rss-feedly-theme-master/feedly*.css tt-rss-feedly-theme-master/feedly/fonts

# RSSHub
wget -qO- https://github.com/DIYgod/ttrss-theme-rsshub/archive/master.tar.gz |
	tar xzvpf - --strip-components=2 -C $DST_DIR/themes.local ttrss-theme-rsshub-master/dist/rsshub.css

## Themes End

cp ${SCRIPT_ROOT}/config.docker.php $DST_DIR/config.php
chmod 644 $DST_DIR/config.php

chown -R $OWNER_UID:$OWNER_GID $DST_DIR \
	/var/log/php8

for d in cache lock feed-icons; do
	chmod 777 $DST_DIR/$d
	find $DST_DIR/$d -type f -exec chmod 666 {} \;
done

$PSQL -c "create extension if not exists pg_trgm"

RESTORE_SCHEMA=${SCRIPT_ROOT}/restore-schema.sql.gz

if [ -r $RESTORE_SCHEMA ]; then
	$PSQL -c "drop schema public cascade; create schema public;"
	zcat $RESTORE_SCHEMA | $PSQL
fi

# this was previously generated
rm -f $DST_DIR/config.php.bak

if [ ! -z "${TTRSS_XDEBUG_ENABLED}" ]; then
	if [ -z "${TTRSS_XDEBUG_HOST}" ]; then
		export TTRSS_XDEBUG_HOST=$(ip ro sh 0/0 | cut -d " " -f 3)
	fi
	echo enabling xdebug with the following parameters:
	env | grep TTRSS_XDEBUG
	cat >/etc/php8/conf.d/50_xdebug.ini <<EOF
zend_extension=xdebug.so
xdebug.mode=develop,trace,debug
xdebug.start_with_request = yes
xdebug.client_port = ${TTRSS_XDEBUG_PORT}
xdebug.client_host = ${TTRSS_XDEBUG_HOST}
EOF
fi

cd $DST_DIR && sudo -E -u app php8 ./update.php --update-schema=force-yes

rm -f /tmp/error.log && mkfifo /tmp/error.log && chown app:app /tmp/error.log

(tail -q -f /tmp/error.log >> /proc/1/fd/2) &

touch $DST_DIR/.app_is_ready

exec /usr/sbin/php-fpm8 --nodaemonize --force-stderr
